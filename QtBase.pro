#-------------------------------------------------
#
# Project created by QtCreator 2017-02-20T21:59:47
#
#-------------------------------------------------

QT       += core network gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = qtbase
CONFIG   += console
CONFIG   -= app_bundle

win32: {
    LIBS += -lWlanapi
}

TEMPLATE = app

SOURCES += \
    io/DirHelper.cpp \
    main.cpp \
    container/RingQueue.cpp \
    enc_dec/wg_ten_to_8.cpp \
    log/log.cpp \
    enc_dec/base64.cpp \
    enc_dec/md5.c \
    system/app_config_setting.cpp \
    net/HttpDownload.cpp \
    net/NetRequest.cpp \
    string/Md5Util.cpp \
    string/charset.cpp \
    system/SingleApp.cpp \
    system/Time.cpp \
    system/systeminfo.cpp \
    ui_component/ColorSelect.cpp \
    ui_component/HotkeyLineEdit.cpp \
    ui_component/ProgressIndicator.cpp \
    ui_component/UIHelper.cpp \
    ui_component/WaveStatusProgress.cpp \
    ui_component/filedialog.cpp

HEADERS += \
    io/DirHelper.h \
    container/HashList.h \
    container/LRUCache.h \
    container/RingQueue.h \
    enc_dec/wg_ten_to_8.h \
    log/log.h \
    enc_dec/base64.h \
    enc_dec/md5.h \
    system/app_config_setting.h \
    net/HttpDownload.h \
    net/HttpHeader.h \
    net/NetRequest.h \
    string/Md5Util.h \
    string/string_ext.h \
    string/charset.h \
    system/SingleApp.h \
    system/Time.h \
    system/systeminfo.h \
    ui_component/ColorSelect.h \
    ui_component/HotkeyLineEdit.h \
    ui_component/ProgressIndicator.h \
    ui_component/UIHelper.h \
    ui_component/WaveStatusProgress.h \
    ui_component/filedialog.h

DISTFILES += \
    README.md
