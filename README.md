## 包含了一些常用的函数模块

所有模块测试代码均在main[测试用例](main.cpp)函数中,每个文件夹有独立的详细介绍，这里只做索引

***
#### Qt
2. 获取Unix格式时间戳
5. 非模态对话框
6. 文件操作
7. 程序配置文件读写（mac：plist）

#### 输入输出（io)


#### 日志（log）
1. log调试

#### 网络相关（net）
1. 下载器（HTTP下载）
2. qt web server
3. 阻塞网络请求
4. 非阻塞睡眠（睡眠途中界面不冻结）

#### Qt UI组件拓展（ui_component）
1. FileDialog，使用原生Qt文件对话框
2. 快捷键Edit框
3. 颜色选择器
4. 波浪进度条

#### 编码解码（enc_dec)
1. base64
2. md5
3. Wiegand 10转8

#### string（string）
1. 字符串拓展（QString << 任意类型）
2. win下字符串相互转换
3. hash相关函数，提供大文件的hash计算

#### 容器（container）
1. 环形缓冲
2. HashList 支持按插入顺序的Hash结构（拥有Hash的随机访问，同时顺序是按照插入的先后顺序排序的。更好的Tessil/ordered-map）
3. LRUCache lru算法的缓存

#### 系统相关（system）
1. 获取MAC地址（windows）
2. 获取IP
3. 获取CPU，操作系统信息
4. 检测操作系统类型（虚拟机、普通机器）
5. 内存(内存总数、可用内存、峰值内存）
6. 磁盘信息（Qt可直接获取）
7. Wlan信息，Wlan是否连接
8. 单程序运行（多平台）
