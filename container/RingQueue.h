#ifndef RING_QUEUE_H_
#define RING_QUEUE_H_

#include <mutex>
#include <vector>

class RingQueue
{
private:
    std::vector<unsigned char> m_buffer;
    std::mutex m_mutex;
    int m_size;
    int m_wpos;
    int m_rpos;
    int m_remain;

public:
    RingQueue(long long size);
    ~RingQueue();

    long long readData(unsigned char *data, long long maxlen);

    long long writeData(const unsigned char *data, long long len);
};


#endif
