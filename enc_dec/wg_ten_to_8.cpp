#include "wg_ten_to_8.h"

#include <QDebug>

QString wgTenTo8(const QString &ten)
{
    QRegExp exp("^[0-9]{10}");
    bool isValid = ten.contains(exp);
    if (ten.length() != 10 || !isValid) {
        return "";
    }

    QString text = ten;
    QString temp = QString::number(text.toLong(),16);
//    qDebug() << temp;
    QString sixByte;
    sixByte.insert(0,temp[temp.size()-1]);
    sixByte.insert(0,temp[temp.size()-2]);
    sixByte.insert(0,temp[temp.size()-3]);
    sixByte.insert(0,temp[temp.size()-4]);
    sixByte.insert(0,temp[temp.size()-5]);
    sixByte.insert(0,temp[temp.size()-6]);
//    qDebug() << sixByte;
    QString header = sixByte.mid(0,2);
    bool ok;
    header =  QString::number(header.toInt(&ok,16));
    while(true){
        if(header.size() < 3){
            header = "0" + header;
        }else{
            break;
        }
    }


    QString tail = sixByte.mid(2);
    tail = QString::number(tail.toInt(&ok,16));
    while(true){
        if(tail.size() < 5){
            tail = "0" + tail;
        }else{
            break;
        }
    }
    QString ulti = header + tail;
    return ulti;
}
