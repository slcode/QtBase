#ifndef DIRHELPER_H
#define DIRHELPER_H

#include <QString>

// 在当前工作路径创建目录
QString createDirWithName(const QString &dir_name);
// 得到一个工作路径下的文件名的绝对路径（如果没有则创建）
QString getAbsoluteFileName(const QString &path,const QString fileName);

QStringList calc_ByteCount_FileCount_DirCount(const QString &path,bool isLink = false);
bool createHideDir(const QString &path);
bool createDirLink(const QString &path,QString dstFullPath);
QStringList findFile(const QString &path,const QString &fileName);

#endif // DIRHELPER_H
