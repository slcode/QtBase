/****************************************************************************
**  @copyright  TinKinG
**  @contract   lsnbing@126.com
**
**  @brief  http下载类
**  @file   httpdownload.h
**  @author TinKinG
**  @date   2017-02-15
**
**
*****************************************************************************/
#ifndef HTTPDOWNLOAD_H
#define HTTPDOWNLOAD_H
#include <QObject>

class QNetworkAccessManager;
class QNetworkReply;
class QFile;

class HttpDownload : public QObject
{
    Q_OBJECT

    QNetworkAccessManager *m_manager;//网络参数
    QNetworkReply * m_reply;

    QString m_url;
    QString m_fileName;
    QString m_dir;

    QFile * m_file;

    qint64 m_recv_size;
    qint64 m_total_size;

    int m_down_size;

    // 返回下载文件大小，返回0-下载错误
    void downloadFile();
public:
    explicit HttpDownload(QObject *parent = 0);
    void setParam(const QString &url,const QString &file,const QString &dir="");

signals:
    void finished(int size);

public slots:
    void launch();

private slots:
    void replyNewDate();
    void replyFinished();
    void replyProgress(qint64 recv,qint64 total);
    void replyError();
};

#endif // HTTPDOWNLOAD_H
