#ifndef HTTPHEADER_H
#define HTTPHEADER_H

#define HTTP_KEY_REFERER             "Referer"
#define HTTP_KEY_X_REQUESTED_WITH    "X-Requested-With"
#define HTTP_VALUE_REQUEST_XML       "XMLHttpRequest"
#define HTTP_VALUE_REQUEST_SWF       "ShockwaveFlash/22.0.0.192"
#define HTTP_USER_AGENT              "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2840.71 Safari/537.36"


#endif // HTTPHEADER_H
