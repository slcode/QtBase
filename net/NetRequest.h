#ifndef NETREQUEST_H
#define NETREQUEST_H

#include <QByteArray>
#include <QMap>

class QNetworkReply;
class QNetworkAccessManager;

typedef QMap<QString,QString> Header;

class NetRequest
{
public:
    NetRequest() = delete;

    // 访问重定向的最终结果
    static QByteArray gAccessResult(QNetworkAccessManager *manager, const QString & url,const Header &refer=Header(), const QByteArray & array=0);
    // 返回网址的直接reply（非重定向）
    static QNetworkReply * gAccessReply(QNetworkAccessManager *manager, const QString & url,const Header &refer = Header(),const QByteArray & array=0);

    // 保存一个完成的路径名（需要完整的URL,仅适合小文件如网页等）
    bool downFile(const QString &url,const QString &absolutePathAndName);
};

#endif // NETREQUEST_H
