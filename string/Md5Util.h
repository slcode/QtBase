#ifndef MD5UTIL_H
#define MD5UTIL_H

#include <QString>
// KEY-MD5码
QString hmacSha1(QByteArray key, QByteArray baseString);
// 获取文件MD5值（md5sum相同计算值）
QString getHashMd5(const QString &filePath);

// 获取文件内容MD5
QString getHashMd5(const QByteArray &data);
std::string GetFileMD5A(const QString& strFilePath);

#endif // MD5UTIL_H
