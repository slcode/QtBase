#ifndef TKG_CHARSET_H_
#define TKG_CHARSET_H_
#include <string>

#ifdef WIN32

std::wstring UT2WC(const char* buf);
std::string WC2UT(const wchar_t* buf);
std::wstring MB2WC(const char* buf);
std::string WC2MB(const wchar_t* buf);
std::string MB2UT(const char* buf);
std::string UT2MB(const char* buf);

#endif

#endif
