/****************************************************************************
**  @copyright  TinKinG
**  @contract   lsnbing@126.com
**
**  @brief  QString << 能力拓展
**  @file   string_ext.h
**  @author TinKinG
**  @date   2016-06-07
**
*****************************************************************************/

#ifndef STRING_EXT_H
#define STRING_EXT_H

// 重载QString的<<操作符
inline QString operator<<(const QString &s,const QString &a){ QString tmp = s; return tmp.append(" ").append(a);}
inline QString operator<<(const QString &s,const char *a){ QString tmp = s;return operator<<(tmp,QString::fromUtf8(a)); }
inline QString operator<<(const QString &s,bool t) { QString tmp = s;return tmp.append(" ").append(t ? "true" : "false"); }
inline QString operator<<(const QString &s,char t) { QString tmp = s; return tmp.append(" ").append(t);}
inline QString operator<<(const QString &s,const QChar t) {QString tmp = s; return operator<<(tmp,t.toLatin1()); }
inline QString operator<<(const QString &s,signed short t) { QString tmp = s;return tmp.append(" ").append(QString::number(t));}
inline QString operator<<(const QString &s,unsigned short t) {QString tmp = s; return tmp.append(" ").append(QString::number(t));}
inline QString operator<<(const QString &s,signed int t) { QString tmp = s;return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,unsigned int t) {QString tmp = s; return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,signed long t) { QString tmp = s;return tmp.append(" ").append((QString::number(t)));  }
inline QString operator<<(const QString &s,unsigned long t) {QString tmp = s; return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,qint64 t) { QString tmp = s;return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,quint64 t) { QString tmp = s;return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,float t) { QString tmp = s;return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,double t) { QString tmp = s;return tmp.append(" ").append(QString::number(t)); }
inline QString operator<<(const QString &s,const QByteArray &t) { QString tmp = s;return tmp.append(" ").append(t); }
inline QString operator<<(const QString &s,const void * t) {QString tmp = s; return tmp.append(" ").append(QString::number(reinterpret_cast<long>(t))); }

template <class T>
inline QString operator<<(const QString &s, const QList<T> &list){
    QString tmp = s;
    tmp.append(" (");
    for (typename QList<T>::size_type i = 0; i < list.count(); ++i) {
        if(i) tmp.append(",");
        tmp.append(operator<<(QString(""),list[i]));
    }
    tmp.append(")");
    return tmp;
}

template <typename T>
inline QString operator<<(const QString &s, const QVector<T> &vec){QString tmp = s; return operator<<(tmp, vec.toList());}

inline QString operator<<(const QString &s,const QStringList &list) { QString tmp = s;return operator<<(tmp,list.toVector());}

template <class aKey, class aT>
inline QString operator<<(const QString &s, const QMap<aKey, aT> &map){
    QString tmp = s;
    tmp.append(" QMap(");
    for (typename QMap<aKey, aT>::const_iterator it = map.constBegin();
         it != map.constEnd(); ++it) {
        tmp.append('(').append(operator<<(QString(""),it.key())).append(",").append(operator<<(QString(""),it.value())).append(')');
    }
    tmp.append(')');
    return tmp;
}

#endif // STRING_EXT_H
