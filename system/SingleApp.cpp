#include "SingleApp.h"

#include <QLocalSocket>

SingleApp::SingleApp(const QString& name, QObject* parent)
    : QObject(parent), mName(name)
{
    connect(&mServer, &QLocalServer::newConnection, this, &SingleApp::onNewConnection);
}

SingleApp::~SingleApp()
{
}

QString SingleApp::getLastErrorString() const
{
    return mServer.errorString();
}

bool SingleApp::start()
{
    mServer.removeServer(mName);
    return mServer.listen(mName);
}

bool SingleApp::hasPreviousInstance()
{
    QLocalSocket socket;
    socket.connectToServer(mName);
    return socket.waitForConnected();
}

void SingleApp::onNewConnection()
{
    emit newInstanceCreated();
}
