#ifndef SINGLEAPPWIN_H
#define SINGLEAPPWIN_H

#include <QObject>
#include <QLocalServer>

class SingleApp : public QObject
{
    Q_OBJECT

public:
    SingleApp(const QString& id, QObject* parent = 0);
    ~SingleApp();

    QString getLastErrorString() const;
    bool start();
    bool hasPreviousInstance();

Q_SIGNALS:
    void newInstanceCreated();

private Q_SLOTS:
    void onNewConnection();

private:
    QLocalServer mServer;
    QString mName;
};

#endif // SINGLEAPPWIN_H
