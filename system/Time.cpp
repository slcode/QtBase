#include "Time.h"
#include <QDateTime>

QString getTime(){
    //js 返回unix毫秒数
#if 0
    QDateTime now = QDateTime::currentDateTime();
    QDateTime _1970 = QDateTime(QDate(1970,1,1),QTime(8,0));
    qint64 base = _1970.msecsTo(now);
#endif
    // or
    qint64 base = QDateTime::currentMSecsSinceEpoch();
    return QString::number(base);
}

QString getUnixTime(int expires_s){
    //返回unix秒数，参数为增加的秒数，未来的expires_s秒数
    QDateTime time;
    if(expires_s == 0)
        time = QDateTime::currentDateTime();
    else{
        time = QDateTime::currentDateTime().addSecs(expires_s);
    }
    QDateTime _1970 = QDateTime(QDate(1970,1,1),QTime(0,0,0));
    qint64 base = _1970.msecsTo(time)/1000;
    return QString::number(base);
}
