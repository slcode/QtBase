#ifndef TIME_H
#define TIME_H

#include <QString>
// 获取1970.1.1 08:00 至今的毫秒数
QString getTime();
// 获取Unix的秒数
QString getUnixTime(int expires_s);

#endif // TIME_H
