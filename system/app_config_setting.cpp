#include <QSettings>
#include <QDir>
#include <QDebug>
#include <QStandardPaths>

#include "app_config_setting.h"

AppConfigSetting::AppConfigSetting()
{
#ifdef WIN32
    settings = new QSettings(QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first() + "\\ES_Live" + "\\config.ini", QSettings::IniFormat);
#else
    settings = new QSettings(QDir::homePath() + "/app.plist", QSettings::NativeFormat);
#endif

    settings->beginGroup("Settings");
}

AppConfigSetting::~AppConfigSetting()
{
    settings->endGroup();
    settings->deleteLater();
}

QVariant AppConfigSetting::get(const QString &key, const QVariant &defaultValue) const
{
    return settings->value(key, defaultValue);
}

void AppConfigSetting::remove(const QString &key)
{
    settings->remove(key);
}

bool AppConfigSetting::contains(const QString &key)
{
    return settings->contains(key);
}

void AppConfigSetting::set(const QString &key, const QVariant &value)
{
    settings->setValue(key, value);
}



