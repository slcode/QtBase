﻿/****************************************************************************
**  @copyright  TinKinG
**  @contract   lsnbing@126.com
**
**  @brief  get System info
**  @file   systeminfo.h
**  @author TinKinG
**  @date   2018-08-08
**
**
*****************************************************************************/
#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#include <QString>
#include <QList>

#ifdef _WIN32
#include <limits.h>
#include <intrin.h>
typedef unsigned __int32  uint32_t;

#else
#include <cstdint>
#endif

/* 获取信息途径：Qt自带、注册表、系统自带工具、系统API获取 */
class SystemInfo
{
    static QList<QPair<QString,QString> > systemInfo();
public:
    SystemInfo();

    static QString getIPAddress();            /* 获取IP地址信息 */
    static QString getCPUInfo();              /* 获取CPU信息 */
    static QString getOSInfo();               /* 获取操作系统信息 */
    static QString getMACAdress();            /* 获取MAC地址 */
    static long long getTotalRamSize();       /* 内存总容量 */
    static long long getAvaliableRamSize();   /* 返回可用内存总数 */
    static long long getRamSize(int pid);     /* 返回pid物理内存占用 */
    static long long getRamSizeOfSelf();      /* 返回当前进程物理内存占用（warning:有几率获取值为空） */
    static long long getPeakRamSize(int pid); /* 返回峰值ram占用 */
    static long long getPeakRamSizeOfSelf();
    static QStringList getWlanNames();        /* 获取wlan信息 */
    static bool isWlanConnected();            /* 是否连接waln */
    static bool isVirtualSystem();            /* 是否是虚拟机 */
    static void storageInfo();
};

#endif // SYSTEMINFO_H
