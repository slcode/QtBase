#include "ColorSelect.h"
#include <QtMath>
#include <QMouseEvent>
#include <QPainter>
#include <QFontMetrics>

#include <QDebug>

#define ALIEN_WH 100
#define COLOR_COUNT 8
#define CIRCLE_REDIUS 10
#define OUTLINE_LENGTH    30  //偏移位置，从笛卡尔坐标系转移到显示屏坐标系

ColorSelect::ColorSelect(QWidget *parent) :
    QWidget(parent),
    m_colors({Qt::red,Qt::black,Qt::green,Qt::blue,Qt::gray,Qt::yellow,Qt::cyan,Qt::magenta,Qt::white}),
    m_points(COLOR_COUNT+1),m_redius(CIRCLE_REDIUS)
{
    resize(90,100);

    setWindowFlags(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

    int thta = 0;
    qreal x0 = OUTLINE_LENGTH,y0 = 0;
    m_maxPoint = QPointF(0,0);
    for(int i = 0; i < COLOR_COUNT; i++,thta += 45){
        qreal x = x0 * qCos(thta/180.0*M_PI) - y0 * qSin(thta/180.0*M_PI);
        qreal y = x0 * qSin(thta/180.0*M_PI) + y0 * qCos(thta/180.0*M_PI);
        m_points[i] = QPointF(x+OUTLINE_LENGTH+CIRCLE_REDIUS,y + OUTLINE_LENGTH+CIRCLE_REDIUS);
        if(m_points[i].y() > m_maxPoint.y())
            m_maxPoint = m_points[i];
    }
    m_points[COLOR_COUNT] = QPointF(OUTLINE_LENGTH + CIRCLE_REDIUS,OUTLINE_LENGTH + CIRCLE_REDIUS);
    setMouseTracking(true);
}

ColorSelect::~ColorSelect()
{
}

void ColorSelect::paintEvent(QPaintEvent * ev)
{
    Q_UNUSED(ev);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    //    p.setRenderHint(QPainter::TextAntialiasing);

    for(int i = 0; i < COLOR_COUNT; ++i){
        p.setPen(m_colors[i]);
        p.setBrush(m_colors[i]);
        p.drawEllipse(m_points[i].x()-CIRCLE_REDIUS,m_points[i].y()-CIRCLE_REDIUS,CIRCLE_REDIUS*2,CIRCLE_REDIUS*2);
    }
    p.setPen(Qt::black);
    p.setBrush(Qt::white);
    int redius = CIRCLE_REDIUS + 2;
    int leftx = m_points[COLOR_COUNT].x() - redius;
    int lefty = m_points[COLOR_COUNT].y() - redius;
    p.drawEllipse(leftx,lefty,2 * redius,2 * redius);

    QString text(u8"选取颜色"+m_colorName);
    QFont font("PingFangSC-Regular",12);
    font.setWeight(50);
    font.setStyleStrategy(QFont::PreferQuality);
    p.setFont(font);
    int width = p.fontMetrics().width(text);
    int height = p.fontMetrics().height();
    p.drawText(QRectF(m_points[COLOR_COUNT].x() - width/2.0,m_maxPoint.y() + CIRCLE_REDIUS + 5,width,height),text);
}

void ColorSelect::mouseMoveEvent(QMouseEvent * ev)
{
    QPoint pos = ev->pos();
    for(int i = 0; i < COLOR_COUNT+1; ++i){
        if( pos.x() > m_points[i].x() - CIRCLE_REDIUS && pos.x() < m_points[i].x() + CIRCLE_REDIUS &&
                pos.y() > m_points[i].y() - CIRCLE_REDIUS && pos.y() < m_points[i].y() + CIRCLE_REDIUS){
            switch (i) {
            case 0:
                m_colorName = ":Red";
                break;
            case 1:
                m_colorName = ":Black";
                break;
            case 2:
                m_colorName = ":Green";
                break;
            case 3:
                m_colorName = ":Blue";
                break;
            case 4:
                m_colorName = ":Gray";
                break;
            case 5:
                m_colorName = ":Yellow";
                break;
            case 6:
                m_colorName = ":BlueGreen";
                break;
            case 7:
                m_colorName = ":Pink";
                break;
            case 8:
                m_colorName = ":White";
                break;
            default:
                break;
            }
            setCursor(Qt::PointingHandCursor);
            update();
            return;
        }
    }
    setCursor(Qt::ArrowCursor);
    QWidget::mouseMoveEvent(ev);
}

void ColorSelect::mousePressEvent(QMouseEvent * ev)
{
    QPoint pos = ev->pos();
    for(int i = 0; i < COLOR_COUNT+1; ++i){
        if( pos.x() > m_points[i].x() - CIRCLE_REDIUS && pos.x() < m_points[i].x() + CIRCLE_REDIUS &&
                pos.y() > m_points[i].y() - CIRCLE_REDIUS && pos.y() < m_points[i].y() + CIRCLE_REDIUS){
            setCursor(Qt::PointingHandCursor);
            emit selectedColor(m_colors[i]);
            this->hide();
            return;
        }
    }
}

void ColorSelect::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    this->hide();
}

void ColorSelect::mouseReleaseEvent(QMouseEvent *ev)
{
    Q_UNUSED(ev);
}

void ColorSelect::showOrHide()
{
    if(this->isHidden()){
        this->show();
    }else{
        this->hide();
    }
}

void ColorSelect::show()
{
    this->setFocus();
    QWidget::show();
}

void ColorSelect::hide()
{
    m_colorName.clear();
    QWidget::hide();
}
