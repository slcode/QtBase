#ifndef COLORELECT_H
#define COLORELECT_H

#include <QWidget>

class ColorSelect : public QWidget
{
    Q_OBJECT
private:
    QVector<QColor> m_colors;
    QVector<QPointF> m_points;
    int m_redius;
    QPointF m_maxPoint;

    // 当前颜色选择器名称和已选择
    QColor m_selected;
    QString m_colorName;
public:
    explicit ColorSelect(QWidget *parent = 0);
    ~ColorSelect();
    void hide();
    void show();

public slots:
    void showOrHide();
protected:
    void paintEvent(QPaintEvent * ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent * ev);
    void mousePressEvent(QMouseEvent * ev);
    void focusOutEvent(QFocusEvent *event);

signals:
    void selectedColor(const QColor & color);
};

#endif // COLORELECT_H
