#include "HotkeyLineEdit.h"

#include <QKeyEvent>
#include <QDebug>

//static QHash<int,QString> f_maps {
//    { 0x30,"0"},
//    { 0x31,"1"},
//    { 0x32,"2"},
//    { 0x33,"3"},
//    { 0x34,"4"},
//    { 0x35,"5"},
//    { 0x36,"6"},
//    { 0x37,"7"},
//    { 0x38,"8"},
//    { 0x39,"9"},
//    { 0x3a,":"},
//    { 0x3b,";"},
//    { 0x3c,"<"},
//    { 0x3d,">"},
//    { 0x3e,"="},
//    { 0x3f,"?"},
//    { 0x40,"@"},
//    { 0x41,"A"},
//    { 0x42,"B"},
//    { 0x43,"C"},
//    { 0x44,"D"},
//    { 0x45,"E"},
//    { 0x46,"F"},
//    { 0x47,"G"},
//    { 0x48,"H"},
//    { 0x49,"I"},
//    { 0x4a,"J"},
//    { 0x4b,"K"},
//    { 0x4c,"L"},
//    { 0x4d,"M"},
//    { 0x4e,"N"},
//    { 0x4f,"O"},
//    { 0x50,"P"},
//    { 0x51,"Q"},
//    { 0x52,"R"},
//    { 0x53,"S"},
//    { 0x54,"T"},
//    { 0x55,"U"},
//    { 0x56,"V"},
//    { 0x57,"W"},
//    { 0x58,"X"},
//    { 0x59,"Y"},
//    { 0x5a,"Z"},
//    { 0x01000030,"F1"},
//    { 0x01000031,"F2"},
//    { 0x01000032,"F3"},
//    { 0x01000033,"F4"},
//    { 0x01000034,"F5"},
//    { 0x01000035,"F6"},
//    { 0x01000036,"F7"},
//    { 0x01000037,"F8"},
//    { 0x01000038,"F9"},
//    { 0x01000039,"F10"},
//    { 0x0100003a,"F11"},
//    { 0x0100003b,"F12"},
//};

HotkeyLineEdit::HotkeyLineEdit(QWidget *parent)
    : QLineEdit(parent)
{
}

HotkeyLineEdit::~HotkeyLineEdit()
{
}

void HotkeyLineEdit::keyPressEvent(QKeyEvent *event)
{
    QString modifierStr;

    /**
         * 键         mac识别符   win识别符
         * shift      Shift
         * control    Meta
         * alt/option Alt
         * command    Ctrl
    */
    switch (event->modifiers()) {
    case Qt::ShiftModifier:
        modifierStr = "Shift+";
        break;
    case Qt::ControlModifier:
        modifierStr = "Ctrl+";
        break;
    case Qt::AltModifier:
        modifierStr = "Alt+";
        break;
    case Qt::MetaModifier:
        modifierStr = "Meta+";
        break;

        /*两键*/
    case (Qt::ShiftModifier | Qt::MetaModifier):
        modifierStr = "Shift+Meta+";
        break;

    case (Qt::ShiftModifier | Qt::AltModifier):
        modifierStr = "Shift+Alt+";
        break;
    case (Qt::ShiftModifier | Qt::ControlModifier):
        modifierStr = "Shift+Ctrl+";
        break;

    case (Qt::MetaModifier | Qt::AltModifier):
        modifierStr = "Meta+Alt+";
        break;

    case (Qt::MetaModifier | Qt::ControlModifier):
        modifierStr = "Meta+Ctrl+";
        break;

    case (Qt::AltModifier | Qt::ControlModifier):
        modifierStr = "Alt+Ctrl+";
        break;

        /*三键*/
#if 0 /* 此键无法使用 */
    case (Qt::ShiftModifier | Qt::MetaModifier | Qt::AltModifier):
        modifierStr = "Shift+Meta+Alt+";
        break;
#endif

    case (Qt::ShiftModifier | Qt::AltModifier | Qt::ControlModifier):
        modifierStr = "Shift+Alt+Ctrl+";
        break;

    case (Qt::MetaModifier | Qt::AltModifier | Qt::ControlModifier):
        modifierStr = "Meta+Alt+Ctrl+";
        break;

        /* 四建 */
    case (Qt::ShiftModifier | Qt::MetaModifier | Qt::AltModifier | Qt::ControlModifier):
        modifierStr = "Shift+Meta+Alt+Ctrl+";
        break;
    default:
        break;
    }

    if (event->key() == 16777248 || event->key() == 16777249 || event->key() == 16777250
            || event->key() == 16777251) {
        return;
    }

    modifierStr += QKeySequence(event->key()).toString();

#ifdef __APPLE__
    if (!modifierStr.endsWith("+") &&modifierStr.contains("+")) {
        emit keySequence(QKeySequence(modifierStr));

        modifierStr = modifierStr.replace("Shift","⇧").replace("Alt","⌥").replace("Meta","⌃").replace("Ctrl","⌘");
        if (modifierStr.contains("+"))
            setText(modifierStr);
    }
#else

#endif
}

