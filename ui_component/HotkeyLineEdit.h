#ifndef HOTKEYLINEEDIT_H
#define HOTKEYLINEEDIT_H

#include <QLineEdit>

class HotkeyLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    HotkeyLineEdit(QWidget *parent = nullptr);
    ~HotkeyLineEdit();

signals:
    void keySequence(QKeySequence seq);

protected:
    virtual void keyPressEvent(QKeyEvent *);
};
#endif // HOTKEYLINEEDIT_H
