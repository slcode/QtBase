#ifndef UIHELPER_H
#define UIHELPER_H

#include <QString>
#include <QWidget>

// GUI非阻塞休眠
void g_mySleep(float sec);
// 显示模态对话框（可选择使用原生的对话框QMessageBox::information...）
int showMessagebox(const QString &title, const QString &text, int icon);
// 显示非模态对话框
void showMessageWithUnmode(const QString &title, const QString &text, int icon, QWidget *parent = 0);


#endif // UIHELPER_H
