/****************************************************************************
**  @copyright  TinKinG
**  @contract   lsnbing@126.com
**
**  @brief  用以解决自带的文件打开后，当父widget被析构的崩溃问题
**  @file   filedialog.h
**  @author TinKinG
**  @date   2018-08-08
**
**
*****************************************************************************/

#ifndef FILEDIALOG_H
#define FILEDIALOG_H
#include <QFileDialog>

class QPushButton;
class QTreeView;
class QListView;
class QTableView;

class FileDialog : public QFileDialog
{
    Q_OBJECT
public:
    FileDialog(QWidget *parent, const QString &path, const QString &title, const QString &filter);
    ~FileDialog();
private:
    QPushButton * m_ok;
    QPushButton * m_cancel;
    QTreeView * m_treeView;//列表
public slots:
    void onClose();
    void onOK();
    void hide();

protected:
    void paintEvent(QPaintEvent * ev);
signals:
    void emitFiles(QStringList);
};

#endif // FILEDIALOG_H
